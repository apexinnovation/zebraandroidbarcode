import { WebPlugin } from '@capacitor/core';
import { ZebraBarcodePlugin } from './definitions';

export class ZebraBarcodeWeb extends WebPlugin implements ZebraBarcodePlugin {
  constructor() {
    super({
      name: 'ZebraBarcode',
      platforms: ['web']
    });
  }

  async echo(options: { value: string }): Promise<{value: string}> {
    console.log('ECHO', options);
    return options;
  }
}

const ZebraBarcode = new ZebraBarcodeWeb();

export { ZebraBarcode };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(ZebraBarcode);
