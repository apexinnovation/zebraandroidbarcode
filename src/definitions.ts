declare module "@capacitor/core" {
  interface PluginRegistry {
    ZebraBarcode: ZebraBarcodePlugin;
  }
}

export interface ZebraBarcodePlugin {
  echo(options: { value: string }): Promise<{value: string}>;

  stopBarcode(): Promise<any>;

  closeBarcode(): Promise<any>;

  startBarcodeScan(): Promise<{ result: string }>;

  vibrate(options: { duration: number }): Promise<any>;
}
