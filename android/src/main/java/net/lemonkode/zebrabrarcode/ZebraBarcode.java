package net.lemonkode.zebrabrarcode;

import android.content.Context;
import android.os.Vibrator;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.zebra.adc.decoder.Barcode2DWithSoft;

import java.io.UnsupportedEncodingException;

import static android.content.Context.VIBRATOR_SERVICE;

@NativePlugin()
public class ZebraBarcode extends Plugin {

    Barcode2DWithSoft barcode2DWithSoft = null;
    String seldata = "ASCII";

    @Override
    public void load() {
        super.load();

        barcode2DWithSoft = Barcode2DWithSoft.getInstance();
    }

    @PluginMethod()
    public void stopBarcode(PluginCall call) {
        if (barcode2DWithSoft != null) {
            barcode2DWithSoft.stopScan();
            call.success();
        } else {
            call.error("Barcode2DWithSoft instance is null.");
        }
    }

    @PluginMethod()
    public void closeBarcode(PluginCall call) {
        if (barcode2DWithSoft != null) {
            barcode2DWithSoft.close();
            call.success();
        } else {
            call.error("Barcode2DWithSoft instance is null.");
        }
    }

    @PluginMethod()
    public void startBarcodeScan(final PluginCall call) {
        final Context context = this.getContext();

        if (barcode2DWithSoft != null) {
            barcode2DWithSoft.scan();
            barcode2DWithSoft.setScanCallback(new Barcode2DWithSoft.ScanCallback() {
                @Override
                public void onScanComplete(int i, int length, byte[] bytes) {
                    if (length < -1) {
                        if (length == -1) {
                            call.error("Scan was cancelled.");
                        } else if (length == 0) {
                            call.error("Scan timed out.");
                        } else {
                            call.error("Failed to scan.");
                        }
                    } else {
                        SoundManage.PlaySound(context, SoundManage.SoundType.SUCCESS);

                        try {
                            JSObject object = new JSObject();
                            object.put("result", new String(bytes, 0, length, seldata));
                            call.success();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();

                            call.error("Something went wrong. Please try again.");
                        }
                    }
                }
            });
        } else {
            call.error("Barcode2DWithSoft instance is null.");
        }
    }

    @PluginMethod()
    public void vibrate(PluginCall call) {
        int duration = call.getInt("duration", 100);

        Vibrator vibrator = (Vibrator) this.getContext().getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(duration);
        call.success();
    }

    @PluginMethod()
    public void echo(PluginCall call) {
        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", value);
        call.success(ret);
    }
}
