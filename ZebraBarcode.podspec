
  Pod::Spec.new do |s|
    s.name = 'ZebraBarcode'
    s.version = '0.0.1'
    s.summary = 'Zebra Barcode 2d scanner Android plugin'
    s.license = 'MIT'
    s.homepage = '1'
    s.author = 'LemonKode Darryl'
    s.source = { :git => '1', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '11.0'
    s.dependency 'Capacitor'
  end